<?php
namespace Drupal\draggable_captcha\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\AjaxResponse;

class DraggableCaptchaController extends ControllerBase {

    static function verify($captcha_sid, $type = '') {
          $request_vars = isset($_REQUEST) ? $_REQUEST : array();

          if (empty($request_vars['action'])) {
            $request_vars['action'] = 'none';
          }

          if ($type) {
            $DraggableCaptchaCodes = 'DraggableCaptchaCodes_'.$type;
            $DraggableCaptchaAnswer = 'DraggableCaptchaAnswer_'.$type;
          } else {
            $DraggableCaptchaCodes = 'DraggableCaptchaCodes';
            $DraggableCaptchaAnswer = 'DraggableCaptchaAnswer';
          }

          switch ($request_vars['action']) {
            case 'verify':
              if (!in_array(mb_substr($request_vars['captcha'], 10), array_values($_SESSION[$DraggableCaptchaCodes]))) {
                _draggable_captcha_log_error($captcha_sid, $request_vars['action']);
                return new JSONResponse(array('status' => 'error'));
              }

              if (mb_substr($request_vars['captcha'], 10) == $_SESSION[$DraggableCaptchaCodes][$_SESSION[$DraggableCaptchaAnswer]]) {
                _draggable_captcha_log_error($captcha_sid, $request_vars['action']);
                return new JSONResponse(array('status' => 'success'));
              }
              else {
                $_SESSION[$DraggableCaptchaCodes] = NULL;
                $_SESSION[$DraggableCaptchaAnswer] = NULL;

                return new JSONResponse(array('status' => 'error'));
              }
              break;
          }

          return new JSONResponse(array('status' => 'error'));
    }

    static function targetImg($type = '') {
      if ($type == 'mini') {
        $imgname = realpath(DRAGGABLE_CAPTCHA_PATH) . "/css/images/bwm-captcha-mini.png";
        $image_buttons = _draggable_captcha_image_buttons($type);
        $target_img_size = array('width' => 33,'height' => 32);
      } else {
        $imgname = realpath(DRAGGABLE_CAPTCHA_PATH) . "/css/images/bwm-captcha.png";
        $image_buttons = _draggable_captcha_image_buttons($type);
        $target_img_size = array('width' => 55,'height' => 56);
      }

      if ($type) {
        $DraggableCaptchaCodes = 'DraggableCaptchaCodes_'.$type;
        $DraggableCaptchaAnswer = 'DraggableCaptchaAnswer_'.$type;
      } else {
        $DraggableCaptchaCodes = 'DraggableCaptchaCodes';
        $DraggableCaptchaAnswer = 'DraggableCaptchaAnswer';
      }

      $im = @imagecreatefrompng($imgname);
      if (!$im) {
        \Drupal::logger("captcha")->error('CAPTCHA', 'Generation of Draggable Captcha target image failed -- source image error!', array());
        exit;
      }

      $key = isset($_SESSION[$DraggableCaptchaAnswer]) ? $_SESSION[$DraggableCaptchaAnswer] : FALSE;
      if (!$key) {
        \Drupal::logger("captcha")->error('Generation of Draggable Captcha target image failed -- key error!.', array());
        exit;
      }

      $destp = imagecreate($target_img_size['width'], $target_img_size['height']);
      imagecopyresampled($destp, $im, 0, 0,
                         $image_buttons[$key]['target']['top'], $image_buttons[$key]['target']['left'],
                         $target_img_size['width'], $target_img_size['height'],
                         $target_img_size['width'], $target_img_size['height']);
      $im = $destp;

      //drupal_add_http_header('Content-Type', 'image/png');
      while (ob_get_level()) ob_end_clean();

      $headers = array('Content-Type' => 'image/png');
      ob_start();
      imagepng($im);
      $binary_response = ob_get_clean();
      return new Response($binary_response , 200, $headers);
    }

    static function generateRefresh($captcha_sid, $type = '') {

      global $base_url;
      $captcha = array();

      // $type is 'ajax' by default
      if ($type == 'ajax') {
        $type = '';
      }

      if ($type == 'mini') {
        $DraggableCaptchaCodes = 'DraggableCaptchaCodes_'.$type;
        $DraggableCaptchaAnswer = 'DraggableCaptchaAnswer_'.$type;
      } else {
        $DraggableCaptchaCodes = 'DraggableCaptchaCodes';
        $DraggableCaptchaAnswer = 'DraggableCaptchaAnswer';
      }

      // Re-setup captcha.
      $captcha_codes = _draggable_captcha_setup($type);
      \Drupal::moduleHandler()->loadInclude('captcha', 'inc', 'captcha');
      $solution = 'draggable_' . $_SESSION[$DraggableCaptchaCodes][$_SESSION[$DraggableCaptchaAnswer]];
      _captcha_update_captcha_session($captcha_sid, $solution);

      $image_buttons = _draggable_captcha_image_buttons($type);

      $captcha_wrapper = '#draggable-captcha' . ($type?"-".$type:"");

      $captcha['form']['captcha_image'] = array(
        '#theme' => 'draggable_captcha' . ($type?"_".$type:""),
        '#image_buttons' => $image_buttons,
        '#captcha_codes' => $captcha_codes,
        '#captcha_sid' => $captcha_sid,
        '#base_path' => base_path(),
        '#attached' => [
            'library' => ["draggable_captcha/default"],
            "drupalSettings" => [
                'draggable_captcha' => [
                        'captcha_sid' => $captcha_sid
                ]
            ]
        ]
      );

      $response = new AjaxResponse();
      $response->addCommand( new ReplaceCommand($captcha_wrapper, \Drupal::service('renderer')->render($captcha)) );

      return $response;

    }


}
